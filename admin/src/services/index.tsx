export interface BaseQuery {
    page?: number;
    size?: number;
    sort_column?: string;
    sort_type?: string;
  }